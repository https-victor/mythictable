﻿using System;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using NodaTime;
using NodaTime.Serialization.JsonNet;
using MythicTable.GameSession;

namespace MythicTable
{
    public class Startup
    {
        private ILogger Logger;

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvcCore(options=>
            {
                options.EnableEndpointRouting = false;
            })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb);
                });

            services.AddCors();
            services.AddSignalR()
                .AddNewtonsoftJsonProtocol();
            services.AddRazorPages();

            services.AddSingleton<IEntityCollection>(new EntityCollection());
            services.AddSingleton<IGameState>(new GameState());
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IWebHostEnvironment env, ILogger<Startup> logger)
        {
            this.Logger = logger;
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                // Make sure CORS policy accepts client running with node.js
                var allowedOrigin = Environment.GetEnvironmentVariable("MTT_ALLOW_ORIGIN");
                if (allowedOrigin != null)
                {
                    app.UseCors(builder =>
                    {
                        builder.WithOrigins(allowedOrigin)
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    });
                }
            }

            app.UseRouting();

            app.UseEndpoints( endpoints => { 
                endpoints.MapHub<LivePlayHub>("/api/live");
                endpoints.MapRazorPages();
                endpoints.MapControllers();
            });
            // Serve static files
            {
                IFileProvider staticFileProvider = new PhysicalFileProvider(
                    Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot"));

                // For development, try serving files from client directory first
                if (env.IsDevelopment())
                {
                    var clientOutputDir = new DirectoryInfo("../../../html/dist/");
                    if (clientOutputDir.Exists) {
                        staticFileProvider = new CompositeFileProvider(
                            new PhysicalFileProvider(clientOutputDir.FullName),
                            staticFileProvider);

                        this.Logger.LogInformation("Serving client files from '{0}'.",
                            clientOutputDir.FullName);
                    }
                    else {
                        this.Logger.LogWarning(
                            "Client build output directory not found at '{0}'. Client assets will not be served.",
                            clientOutputDir.FullName);
                    }
                }

                app.Use(async (context, next) =>
                {
                    // Redirect all unknown paths to default file so that client-side SPA works
                    var requestPath = context.Request.Path;
                    var isAllowedStaticFile = requestPath.StartsWithSegments("/css")
                        || requestPath.StartsWithSegments("/js")
                        || requestPath.StartsWithSegments("/static")
                        || requestPath.StartsWithSegments("/.well-known")
                        || requestPath == "/favicon.ico";

                    if (!isAllowedStaticFile)
                    {
                        context.Request.Path = new PathString("/index.html");
                    }

                    await next();
                });

                app.UseStaticFiles(
                    new StaticFileOptions {FileProvider = staticFileProvider});
            }
        }
    }
}
