﻿using System;
using System.Threading.Tasks;
using Dice;
using Microsoft.AspNetCore.SignalR;
using MythicTable.GameSession;

namespace MythicTable
{
    public class LivePlayHub : Hub<ILiveClient>
    {
        public IEntityCollection EntityCollection { get; }
        public IGameState GameState { get; }

        public LivePlayHub(IEntityCollection entities, IGameState gamestate)
        {
            EntityCollection = entities;
            GameState = gamestate;
        }

        // TODO: #17: A difference between the GameState on the server and the GameState for player clients exists. This means a delta should not just be broadcast once applied
        // The GM client will resolve actions and submit 1 delta for the players and another delta for the server
        [HubMethodName("submitDelta")]
        public async Task<bool> RebroadcastDelta(SessionDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await EntityCollection.ApplyDelta(delta.Entities);

            if (success)
            {
                await Clients.All.ConfirmDelta(delta);
            }

            return success;
        }

        [HubMethodName("rollDice")]
        public async Task<bool> RollDice(DiceRoll diceRoll)
        {

            try
            {
                RollResult result = Roller.Roll(diceRoll.Formula);
                diceRoll.Result = result.ToString();
            }
            catch (DiceException)
            {
                diceRoll.Result = "Invalid dice roll";
            }

            await Clients.All.ReceiveDiceResult(diceRoll);
            return true;
        }

        [HubMethodName("submitDeltaTemp")]
        public async Task<bool> RebroadcastDeltaTemp(SessionOpDelta delta)
        {
            // TODO #6: This should call the GameState ApplyDelta. EntityCollection functionality should be ported and generalised.
            var success = await GameState.ApplyDelta(delta);

            if (success)
            {
                await Clients.All.ConfirmOpDelta(delta);
            }

            return success;
        }

        [HubMethodName("submitUndo")]
        public async Task<bool> RebroadcastUndo(string undoPlaceHolder)
        {
            var success = await GameState.Undo(undoPlaceHolder);

            if (success)
            {
                await Clients.All.Undo();
            }

            return success;
        }

        [HubMethodName("submitRedo")]
        public async Task<bool> RebroadcastRedo(string redoPlaceholder)
        {
            var success = await this.GameState.Redo(redoPlaceholder);

            if (success)
            {
                await Clients.All.Redo();
            }

            return success;
        }
        
    }
}
