﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MythicTable.GameSession;

namespace MythicTable.Controllers
{
    [ApiController]
    public class GameSessionController : Controller
    {
        private readonly IEntityCollection Entities;

        public GameSessionController(IEntityCollection entities)
        {
            this.Entities = entities;
        }

        
        [HttpGet("/api/s/{sessionId}/entities")]
        public Task<IEnumerable<object>> GetAllEntities(string sessionId)
        {
            return this.Entities.GetEntities();
        }
    }
}