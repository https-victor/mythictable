using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.SignalR;
using Moq;
using MythicTable;
using MythicTable.GameSession;
using Xunit;

namespace LivePlayTests
{
    // TODO #6: Test GameState
    public class LivePlayHubTests
    {
        [Fact]
        public async Task GameStepIsSubmittedToStateManagerOnce()
        {
            var stateMock = new Mock<IEntityCollection>();
            var gameStateMock = new Mock<IGameState>();
            var hub = new LivePlayHub(stateMock.Object, gameStateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(Mock.Of<ILiveClient>());
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            stateMock.Verify(
                entities => entities.ApplyDelta(
                    It.Is<IEnumerable<EntityOperation>>(step => step == submittedStep.Entities)),
                Times.Once);
        }

        [Fact]
        public async Task GameStepIsRepeatedToAllClientsOnce()
        {
            var stateMock = new Mock<IEntityCollection>();
            var gameStateMock = new Mock<IGameState>();
            var hub = new LivePlayHub(stateMock.Object, gameStateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(true));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Once());
        }

        [Fact]
        public async Task FailedSubmissionsAreNotRepeated()
        {
            var stateMock = new Mock<IEntityCollection>();
            var gameStateMock = new Mock<IGameState>();
            var hub = new LivePlayHub(stateMock.Object, gameStateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            stateMock
                .Setup(s => s.ApplyDelta(It.IsAny<IEnumerable<EntityOperation>>()))
                .Returns(Task.FromResult(false));

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);
            allClientsMock
                .Setup(a => a.ConfirmDelta(It.IsAny<SessionDelta>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var submittedStep = new SessionDelta
            {
                Entities = new[]
                {
                    new EntityOperation
                    {
                        EntityId = "obj1",
                        Patch = new JsonPatchDocument().Add("/a/b/c", "foo"),
                    },
                },
            };

            await hub.RebroadcastDelta(submittedStep);

            allClientsMock.Verify(
                c => c.ConfirmDelta(It.Is<SessionDelta>(step => step == submittedStep)),
                Times.Never);
        }

        [Fact]
        public async Task ValidDiceRollsAreExecuted()
        {
            var stateMock = new Mock<IEntityCollection>();
            var gameStateMock = new Mock<IGameState>();
            var hub = new LivePlayHub(stateMock.Object, gameStateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);

            allClientsMock
                .Setup(a => a.ReceiveDiceResult(It.IsAny<DiceRoll>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var diceRoll = new DiceRoll
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "1d5"
            };

            await hub.RollDice(diceRoll);

            allClientsMock.Verify(
                c => c.ReceiveDiceResult(It.Is<DiceRoll>(rolled => rolled.Result != "Invalid dice roll" && rolled.ClientId == "123" && rolled.SessionId == "test" && rolled.Timestamp == 5678)),
                Times.Once());
        }

        [Fact]
        public async Task InvalidDiceRollsAreNotExecuted()
        {
            var stateMock = new Mock<IEntityCollection>();
            var gameStateMock = new Mock<IGameState>();
            var hub = new LivePlayHub(stateMock.Object, gameStateMock.Object);
            var clientsMock = new Mock<IHubCallerClients<ILiveClient>>();
            var allClientsMock = new Mock<ILiveClient>();

            clientsMock.Setup(m => m.All).Returns(allClientsMock.Object);

            allClientsMock
                .Setup(a => a.ReceiveDiceResult(It.IsAny<DiceRoll>()))
                .Returns(Task.CompletedTask);
            hub.Clients = clientsMock.Object;

            var diceRoll = new DiceRoll
            {
                ClientId = "123",
                SessionId = "test",
                Timestamp = 5678,
                Formula = "aaaaaaaaa"
            };

            await hub.RollDice(diceRoll);

            allClientsMock.Verify(
                c => c.ReceiveDiceResult(It.Is<DiceRoll>(rolled => rolled.Result == "Invalid dice roll" && rolled.ClientId == "123" && rolled.SessionId == "test" && rolled.Timestamp == 5678)),
                Times.Once());
        }
    }
}
