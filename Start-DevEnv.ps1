$clientPath = "./html"
$clientPort = 5000
$clientUrl = "http://localhost:${clientPort}/"

$serverPath = "./server/src/MythicTable"
$serverPort = 5001

function Start-VueCli() {
	if (Test-NetConnection "localhost" -Port $clientPort -InformationLevel quiet) {
		Write-Warning "Client development environment is already running at http://localhost:${clientPort}"
		return
	}

	Start-Process "npm" -ArgumentList "run","serve" -WorkingDirectory $clientPath
}

function Start-DotNetServer() {
	if (Test-NetConnection "localhost" -Port $serverPort -InformationLevel quiet) {
		Write-Warning "Server development environment is already running at https://localhost:${serverPort}"
		return $true
	}

	Start-Process "dotnet" -ArgumentList "watch","run" -WorkingDirectory $serverPath
}

Start-DotNetServer
Start-VueCli

# Wait for client server to accept connections
$timeout = (Get-Date).AddMinutes(1)
do {
	if ((Get-Date) -gt $timeout) {
		Write-Error "vue-cli did not start or is refusing conneections" `
			-RecommendedAction "Check the output of the npm console for details"
	}

	Start-Sleep -Seconds 2;
} until (Test-NetConnection "localhost" -Port $clientPort -InformationLevel quiet)

Write-Host "Opening client (${clientUrl})..."
Start-Process $clientUrl
