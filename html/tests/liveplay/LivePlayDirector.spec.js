import LivePlayDirector from '@/live/LivePlayDirector';
import GameStateStore from '@/store/GameStateStore';
import LivePlayState from '@/live/LivePlayState';
import Vuex from 'vuex';
import { createLocalVue } from '@vue/test-utils';
import _ from 'lodash';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('LivePlayDirector', () => {
    let store;
    let director;
    beforeEach(() => {
        store = new Vuex.Store({
            modules: {
                live: _.cloneDeep(LivePlayState),
                gamestate: _.cloneDeep(GameStateStore),
            },
        });
        director = new LivePlayDirector(store);
    });
    describe('General state', () => {
        beforeEach(() => {
            store.commit('live/setClientId', '123');
            store.commit('live/setSessionId', '456');
        });
        it('store has this director', () => {
            expect(store.state.live.director).toBe(director);
        });
        it('this director has a store', () => {
            expect(director).toBe(store.state.live.director);
        });
    });
    describe('function calls', () => {
        it('A received roll goes into the gamestate store', () => {
            const diceResult = {
                timestamp: 123,
                clientId: '456',
                sessionId: 'test',
                formula: '1d4',
                result: '1d4 => 1! => 1',
            };
            director.onReceiveDiceResult(diceResult);
            expect(store.state.gamestate.global.rollLog[0]).toBe(diceResult);
        });
        it('Multiple received rolls go into the gamestate store in the correct order', () => {
            const diceResult = {
                timestamp: 123,
                clientId: '456',
                sessionId: 'test',
                formula: '1d4',
                result: '1d4 => 1! => 1',
            };
            const diceResult2 = {
                timestamp: 1222,
                clientId: '456',
                sessionId: 'test',
                formula: '1d4',
                result: '1d4 => 2 => 2',
            };
            director.onReceiveDiceResult(diceResult);
            director.onReceiveDiceResult(diceResult2);
            expect(store.state.gamestate.global.rollLog[0]).toBe(diceResult);
            expect(store.state.gamestate.global.rollLog[1]).toBe(diceResult2);
        });
    });
});
