import RollLog from '@/render/diceroller/RollLog';
import GameStateStore from '@/store/GameStateStore';
import _ from 'lodash';

import Vuex from 'vuex';
import { shallowMount, createLocalVue } from '@vue/test-utils';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('RollLog', () => {
    let store;
    let rollLogComponent;
    const sampleDiceRoll = {
        timestamp: 123,
        clientId: '456',
        sessionId: 'test',
        formula: '1d4',
    };
    const sampleDiceRoll2 = {
        timestamp: 124,
        clientId: '456',
        sessionId: 'test',
        formula: '1d8',
    };
    beforeEach(() => {
        store = new Vuex.Store({
            modules: {
                gamestate: _.cloneDeep(GameStateStore),
            },
        });

        rollLogComponent = shallowMount(RollLog, { store, localVue }).vm;
    });
    it('has a roll log', () => {
        const log = rollLogComponent.rollLog;
        expect(log).toEqual([]);
    });
    it('has the correct rolls in the log', () => {
        store.state.gamestate.global.rollLog = [sampleDiceRoll, sampleDiceRoll2];
        const log = rollLogComponent.rollLog;
        expect(log).toEqual([sampleDiceRoll, sampleDiceRoll2]);
    });
    it('puts the rolls into a text output', () => {
        store.state.gamestate.global.rollLog = [sampleDiceRoll, sampleDiceRoll2];
        const text = rollLogComponent.textOutput;
        expect(text.length).toBe(2);
    });
});
