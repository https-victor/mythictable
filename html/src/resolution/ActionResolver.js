// Functionality to parse and execute action
// TODO: #13: Design a flexible action model
// TODO: #15: Convert to typescript
import store from '../store/GameStateStore';

const ActionResolver = {
    resolve(action) {
        // TODO: #13, #14: Implement function that deals with possible required resolutions like dicerolls, moves,...
        // and creates a delta out of them.
        let delta = [];

        if (action.id == 'increment') {
            let entityID = action.entity.id;
            let path = '/entities/' + entityID + '/counter';
            // Treat an undefined counter as 0
            let change = store.state.entities[entityID].counter
                ? { op: 'replace', path: path, value: store.state.entities[entityID].counter + 1 }
                : { op: 'add', path: path, value: 1 };
            delta.push(change);
        } else if (action.id == 'decrement') {
            let entityID = action.entity.id;
            let path = '/entities/' + entityID + '/counter';
            // Treat an undefined counter as 0
            let change = store.state.entities[entityID].counter
                ? { op: 'replace', path: path, value: store.state.entities[entityID].counter - 1 }
                : { op: 'add', path: path, value: -1 };
            delta.push(change);
        } else if (action.id == null) {
            // just a normal patch
            delta.push(action);
        }

        return delta;
    },
    // eslint-disable-next-line no-unused-vars
    parse(action) {
        // TODO: #13, #14: Function that takes the action syntax and parses it for elements describing gamestate paths, dicerolls, ...
    },
};

export default ActionResolver;
