﻿import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';

import LivePlayState from './live/LivePlayState';
import GameStateStore from './store/GameStateStore';

import actions from './ruleset/experiment/actions';

Vue.use(VueRouter);
Vue.use(Vuex);

const LivePlay = () => import('./LivePlay.vue');

// FIXME: find a better place to load in the ruleset than main.js
const ruleset = {
    actions: actions,
};
GameStateStore.state.ruleset = ruleset;

const store = new Vuex.Store({
    modules: {
        live: LivePlayState,
        gamestate: GameStateStore,
    },
    state: {},
});

store.commit('live/setClientId', Math.floor(Math.random() * 10000));

const router = new VueRouter({
    routes: [
        { path: '*', redirect: '/play' },
        { path: '/play/:sessionId/:sceneId', component: LivePlay, props: true },
        { path: '/play', redirect: '/play/$demo/stronghold' },
        { path: '/$debug/*', component: () => import('./debug/DebugPage.vue') },
        { path: '/$debug', redirect: '/$debug/' },
    ],
    mode: 'history',
});

// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: '#app',
    store,
    router,
    render: h => h('router-view'),
});

export { store };
