export default class GridCornerRenderer {
    constructor() {}

    /**
     *
     * @param {Object} config.blueprint - Grid type specific blueprint
     * @param {number} config.pixelRatio - Max pixel ratio allowed
     * @param {HTMLCanvasElement} canvas
     */
    render(config, canvas) {
        const width = Math.floor(config.blueprint.width * config.pixelRatio);
        const height = Math.floor(config.blueprint.height * config.pixelRatio);

        canvas.width = width;
        canvas.height = height;

        let crossSize = Math.floor(width * 0.3);
        if (crossSize % 2 === 0) {
            crossSize += 1;
        }
        const offset = Math.ceil(crossSize / 2);

        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, width, height);

        ctx.strokeStyle = '#000000ff';
        ctx.moveTo(offset - 1 + 0.5, 0);
        ctx.lineTo(offset - 1 + 0.5, crossSize);
        ctx.moveTo(0, offset - 1 + 0.5);
        ctx.lineTo(crossSize, offset - 1 + 0.5);
        ctx.stroke();

        return {
            image: canvas,
            offset: { x: offset, y: offset },
            scale: config.blueprint.width / canvas.width,
        };
    }
}
