const LivePlayState = {
    namespaced: true,
    state: {
        connected: false,
        sessionId: null,
        clientId: null,
        director: null,
    },
    mutations: {
        setDirector(state, director) {
            state.director = director;
        },
        setSessionId(state, sessionId) {
            state.sessionId = sessionId;
        },
        setClientId(state, clientId) {
            state.clientId = clientId;
        },
    },
    actions: {},
};

export default LivePlayState;
